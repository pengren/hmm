import numpy as np

def probOGivenS(x, mu, sigma):
	gamma = x - mu
	if gamma < 0:
		gamma = min(abs(x-mu), abs(x-mu+2*np.pi))
	else:
		gamma = min(abs(x-mu), abs(x-mu-2*np.pi))
	return np.log(np.exp(-((np.power(gamma, 2))/(2*np.power(sigma, 2)))) / (np.sqrt(2*np.pi)*sigma))
