### ReadMe

This is the code for evaluation of the HMM offline model and the HMM online model. 

To run the test, simply open the project using Xcode, then run the main.swift.

Input:

1. file_name_list.csv

   a file that contains all the input file names and path

2. csv data in WC folder

output:

HMM_res

After run the test, feel free to use the maybe_the_final.py to see the result. (maybe_the_final.py is in the lstm_for_turn_detection project)