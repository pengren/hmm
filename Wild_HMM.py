# Try to implement the EM algorithm for training HMM, hopefully, I can do that~
import copy
import numpy as np
import HMM_util as util
import pdb

def initial_transition_prob(self):
	self.P0 =  0.9988839
	self.P45 = 0.000151
	self.P90 = 0.000959
	self.P180 = 0.000050
	self.Pd = 0.490604
	self.delta = 0.00913624
	self.sigma = 0.352632
	self.States = [0, np.pi/4, np.pi/2, (3/4)*np.pi, np.pi, \
	-(3/4)*np.pi, -np.pi/2, -np.pi/4, self.delta, -self.delta]
	self.N = len(self.States)
	# here we may have a problem, why so many 45?
	sumOfProbs = self.P0+self.P45+self.P90+self.P45+self.P180+self.P45+self.P90+self.P45+self.Pd+self.Pd
	self.P0 = self.P0/sumOfProbs
	self.P45 = self.P45/sumOfProbs
	self.P90 = self.P90/sumOfProbs
	self.P180 = self.P180/sumOfProbs
	self.Pd = self.Pd/sumOfProbs
	# transition probability
	self.A = [[self.P0,self.P45,self.P90,self.P45,self.P180,self.P45,self.P90,self.P45,self.Pd,self.Pd],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0],
			[1,0,0,0,0,0,0,0,0,0]]
	self.A = np.asarray(self.A)

class HMM():

	def __init__(self):
		# initial_transition prob
		initial_transition_prob(self)

	def train(self, iterations, observation, verbose = True):
		self.T = len(observation)
		self.observation = observation
		for index in range(iterations):
			# train the HMM
			if verbose:
				print("Iteration: {}".format(index))
			self.expectation()
			self.maximization()
			pdb.set_trace()

	# Do the expectation step
	def expectation(self):
		self.forward()
		self.backward()
		self.obtain_psi()
		self.obtain_gamma()

	def maximization(self):
		for i in range(self.N):
			if i==0:
				for j in range(self.N):
					self.A[i, j] = self.update_through_psi(i ,j)

	def update_through_psi(self, i, j):
		nominator = 0
		for t in range(self.T - 1):
			nominator += self.psi[t, i, j]
		
		denominator = 0
		for t in range(self.T - 1):
			for k in range(self.N):
				denominator += self.psi[t, i, k]

		return nominator/float(denominator)

	def forward(self):
		# create a probability matrix forward[N,T]
		self.forward_mat = np.zeros((self.N, self.T))
		self.statesSumPast = np.zeros((self.N, self.T))
		self.forward_fin_prob = 0
		# state from 0 to N-1
		# T from 0 to T-1
		# initialization step
		for state_index in range(self.N):
			# The begin state is always zero, right? 
			# No matter what we observed
			First_observation = self.observation[0]
			# Maybe also learn the bias here? 
			# Maybe this part should be 1
			self.forward_mat[state_index, 0] = util.probOGivenS(First_observation, 0, self.sigma) if state_index is 0 else 0
		# recursion step time: 1:T-1
		for time_step in range(1, self.T):
			current_observation = self.observation[time_step]
			assert time_step<self.T, "wrong understand"
			for state_index in range(self.N):
				All_prob = []
				best_prob = -1
				best_statesSum = 0
				if False:
					previous_state_index = 0
					# calculate current state sum
					statesSum = self.statesSumPast[previous_state_index][time_step - 1] + self.States[state_index]
					prob = self.forward_mat[previous_state_index, time_step - 1] * \
					self.A[previous_state_index, state_index] * util.probOGivenS(current_observation, \
						statesSum, self.sigma)
					if prob>best_prob:
						best_prob = prob
						best_statesSum = statesSum
				else:
					for previous_state_index in range(self.N):
						# calculate current state sum
						statesSum = self.statesSumPast[previous_state_index][time_step - 1] + self.States[state_index]
						prob = self.forward_mat[previous_state_index, time_step - 1] * \
						self.A[previous_state_index, state_index] * util.probOGivenS(current_observation, \
							statesSum, self.sigma)
						All_prob.append(prob)
						if prob>best_prob:
							best_prob = prob
							best_statesSum = statesSum
				self.statesSumPast[state_index, time_step] = best_statesSum
				self.forward_mat[state_index, time_step] = np.sum(All_prob)
		for state_index in range(self.N):
			self.forward_fin_prob += self.forward_mat[state_index, self.T - 1]

	def backward(self):
		self.backward_mat = np.zeros((self.N, self.T))
		self.statesSumFuture = np.zeros((self.N, self.T))
		self.backward_fin_prob = 0
		# we could use label to initialize the statesSumPast, but maybe the following method will work
		for state_index in range(self.N):
			self.statesSumFuture[state_index, self.T - 1] = self.statesSumPast[state_index, self.T - 1]
		# initialize step, T-1
		for state_index in range(self.N):
			self.backward_mat[state_index, self.T - 1] = 1
		# recursion step time: T-2:0
		for time_step in range(self.T-2, -1, -1):
			next_observation = self.observation[time_step + 1]
			for state_index in range(self.N):
				All_prob = []
				best_prob = -1
				best_statesSum = 0
				if False:
					next_state_index = 0
					# calculate next state sum for observation at t + 1
					statesSum = self.statesSumFuture[next_state_index, time_step + 1]
					prob = self.backward_mat[next_state_index, time_step + 1] * \
					self.A[state_index, next_state_index] * util.probOGivenS(next_observation, \
						statesSum, self.sigma)
					# obtain assume state result sum
					statesSum -= self.States[state_index]
					All_prob.append(prob)
					if prob>best_prob:
						best_prob = prob
						best_statesSum = statesSum
				else:
					for next_state_index in range(self.N):
						# calculate next state sum for observation at t + 1
						statesSum = self.statesSumFuture[next_state_index, time_step + 1]
						prob = self.backward_mat[next_state_index, time_step + 1] * \
						self.A[state_index, next_state_index] * util.probOGivenS(next_observation, \
							statesSum, self.sigma)
						# obtain assume state result sum
						statesSum -= self.States[state_index]
						All_prob.append(prob)
						if prob>best_prob:
							best_prob = prob
							best_statesSum = statesSum
				self.statesSumFuture[state_index, time_step] = best_statesSum
				self.backward_mat[state_index, time_step] = np.sum(All_prob)
		First_observation = self.observation[0]
		self.backward_fin_prob = util.probOGivenS(First_observation, 0, self.sigma) * self.backward_mat[0,0]

	def obtain_psi(self):
		self.psi = np.zeros((self.T - 1, self.N, self.N))
		for t in range(self.T-1):
			for i in range(self.N):
				for j in range(self.N):
					self.psi[t, i, j] = self.calculate_single_psi(t, i, j)

	def calculate_single_psi(self, t, i ,j):
		alpha_t_i = self.forward_mat[i, t]
		beta_t_plus_j = self.backward_mat[j, t+1]
		trans_a_i_j = self.A[i, j]
		statesSum = self.statesSumPast[j, t + 1]
		next_observation = self.observation[t + 1]
		emission_prob_b = util.probOGivenS(next_observation, statesSum, self.sigma)
		nominator = alpha_t_i * trans_a_i_j * emission_prob_b * beta_t_plus_j
		denominator = 0
		for index in range(self.N):
			denominator += self.forward_mat[index, t] * self.backward_mat[index, t]
		return nominator/float(denominator)

	def obtain_gamma(self):
		self.gamma = np.zeros((self.N, self.T))
		for time_step in range(self.T):
			current_sum = 0
			for state_index in range(self.N):
				current_sum += self.forward_mat[state_index, time_step] * \
				self.backward_mat[state_index, time_step]
			for state_index in range(self.N):
				self.gamma[state_index, time_step] = (self.forward_mat[state_index, time_step] * \
				self.backward_mat[state_index, time_step])/current_sum

if __name__ == '__main__':
	# obtain a single observation
	observation = np.zeros(20)
	test = HMM()
	test.train(iterations = 10, observation = observation)
