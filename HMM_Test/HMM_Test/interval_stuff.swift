//
//  interval_stuff.swift
//  HMM_Test
//
//  Created by renpeng on 7/2/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class Interval_stuff {
    init() {
    }
    public func interval_to_info(_ interval_seq: [Double]) -> [(start: Int, end: Int)]{
        // turn interval to info type
        var res:[(start: Int, end: Int)] = []
        var start = 0
        var in_interval = false
        for index in 0..<interval_seq.count{
            let label = interval_seq[index]
            
            if label>=1 && in_interval==false{
                res.append((start: start, end: index))
                in_interval = true
            }
            if label==0 && in_interval==true{
                start = index
                in_interval = false
            }
        }
        if in_interval==false{
            res.append((start: start, end: interval_seq.count))
        }
        while res.count<20 {
            res.append((start: 0, end: 0))
        }
        return res
    }
    
    public func obtain_two_kinds_of_interval(_ current_interval_info: [(start: Int, end: Int)]) -> ([(start: Int, end: Int)], [(start: Int, end: Int)]){
        var during_turn_interval:[(start: Int, end: Int)] = []
        for index in 0..<current_interval_info.count{
            if index==0{
                continue
            }
            if current_interval_info[index].start == current_interval_info[index].end{
                continue
            }
            during_turn_interval.append((start: current_interval_info[index-1].end, end: current_interval_info[index].start))
        }
        while during_turn_interval.count<20 {
            during_turn_interval.append((start: 0, end: 0))
        }
        let between_turn_interval = current_interval_info
        return (between_turn_interval, during_turn_interval)
    }
    
    public func turn_origin_interval_set_to_info(_ interval: [Double]) -> ([(start: Int, end: Int)], [(start: Int, end: Int)]){
        let info = interval_to_info(interval)
        let (be_info, dur_info) = obtain_two_kinds_of_interval(info)
        return (be_info, dur_info)
    }
    
    public func turn_interval_set_to_info(_ interval: [Double]) -> [(start: Int, end: Int)]{
        let info = interval_to_info(interval)
        return info
    }
}
