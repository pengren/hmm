//
//  turn_detection.swift
//  HMM_Test
//
//  Created by renpeng on 7/2/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class Turn_detector {
    
    init() {
    }
    public func Intuitive_interval_detection(_ data_seq: [Double]) -> [Double]{
        // Detect turn using two thresholds
        let basic_threshold = 0.030
        var inter_threshold = 0.015
        var label_seq = [Double]()
        var inside_interval = false
        var flip_label = 0.0
        var max_value_within = 0.0
        for index in 0..<data_seq.count{
            let data = data_seq[index]
            if data>basic_threshold && inside_interval == false{
                inside_interval = true
                flip_label = 1.0
                max_value_within = 0.0
                // Reinitialize the leaving threshold
                inter_threshold = 0
            }
            else if data<inter_threshold && inside_interval == true{
                inside_interval = false
                flip_label = 0.0
                // From this part, go back and let the interval begins at the threshold larger than inter_threshold
                for i in 1..<index{
                    let cur_i = index - i
                    if data_seq[cur_i] > inter_threshold{
                        label_seq[cur_i] = 1.0
                    }
                    else{
                        break
                    }
                }
            }
            
            if inside_interval{
                if data > max_value_within{
                    max_value_within = data
                }
                inter_threshold = max(max_value_within/5.0, 0.015)
            }
            label_seq.append(flip_label)
        }
        return label_seq
    }
    
    public func Interval_label_process(_ interval_label: [Double]) -> [Double]{
        // First, get the interval
        let during_info_erase_window = 5
        let between_info_erase_window = 25
        
        let IS = Interval_stuff()
        let (_, Grou_dur_info) = IS.turn_origin_interval_set_to_info(interval_label)
        var cur_res = interval_label
        // filter the Interval
        for index in 0..<Grou_dur_info.count{
            let dur_info = Grou_dur_info[index]
            if dur_info.start == 0 && dur_info.end == 0{
                break
            }
            if dur_info.end - dur_info.start <= during_info_erase_window{
                for j in dur_info.start..<dur_info.end{
                    cur_res[j] = 0.0
                }
            }
        }
        let (Grou_bet_info, _) = IS.turn_origin_interval_set_to_info(cur_res)
        
        for index in 0..<Grou_bet_info.count{
            let bet_info = Grou_bet_info[index]
            if bet_info.start == 0 && bet_info.end == 0{
                break
            }
            if bet_info.end - bet_info.start <= between_info_erase_window{
                for j in bet_info.start..<bet_info.end{
                    cur_res[j] = 1.0
                }
            }
        }
        
//        // Work around: if last interval label is 1, we simply add an non-1 interval
//        for interval_seq in cur_res{
//
//        }
        
        return cur_res
    }
}
