//
//  Batch_process_HMM.swift
//  HMM_Test
//
//  Created by renpeng on 2/13/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class Batch_process {
    var file_list_path = ""
    var CSV_handler:CSV_oper
    var file_list:[String] = []
    init(file_list_path: String) { // Constructor
        self.file_list_path = file_list_path
        self.CSV_handler = CSV_oper()
        }
    // Read in file list from self.file_list_path
    public func obtain_file_list() -> Void {
        self.file_list = self.CSV_handler.Read_in_CSV(file_path: self.file_list_path, row_num: 0)
    }
    // Process HMM test in batch
    public func batch_process() -> Void{
        obtain_file_list()
        for file_path in self.file_list{
            let KL_tester = KL_Online_test(file_path)
            KL_tester.load_data()
            KL_tester.preprocess_data()
            // KL_tester.run_HMM_Online()
        }
    }
    
}
