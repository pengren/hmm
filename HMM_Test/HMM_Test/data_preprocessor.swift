//
//  data_preprocessor.swift
//  HMM_Test
//
//  Created by renpeng on 7/2/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class data_preprocessor {
    var mean_filter_window_size: Int
    var stddev_gen_window_size: Int
    var bias_estimation_window_size: Int
    var start_data: [Double] // Used to start the whole process
    var filtered_data: [Double] // the data processed by many filters
    var Found_real_start_point: Bool
    var time_step: Int
    var zero_begin_bias: Double
    var real_begin_time_step: Int
    init(_ start_data: [Double]) { // Constructor
        // Set parameters
        self.bias_estimation_window_size = 20
        self.mean_filter_window_size = 30
        self.stddev_gen_window_size = 15
        self.start_data = start_data
        self.Found_real_start_point = false
        self.time_step = 0
        self.real_begin_time_step = 0
        self.zero_begin_bias = 0.0
        self.filtered_data = []
    }
    
    public func load_single_data(_ single_input_data: Double) -> Void{
        // Run this to load in a single point of data
        // trick: the zero bias is just moved at here...
        self.start_data.append(single_input_data - self.zero_begin_bias)
        self.time_step += 1
    }
    
    public func process_input_data() -> Void{
        // Preprocess those data
        // First, locate the real start position
        if self.Found_real_start_point == false{
            locate_real_start_point()
            print("Still searching for real start point...")
            return
        }
        // Then, process the input data
        if self.start_data.count >= 195{
            // When there is enough previous data, use it
            let prepared_data = Array(self.start_data.suffix(195))
            self.filtered_data.append(contentsOf: delay_filters_package(prepared_data, use_more_data: true))
        }
        else{
            // If previous data is not enough, use 0. for padding
            let padding_length = 195 - self.start_data[self.real_begin_time_step...].count
            let zero_before = Array(repeating: 0.0, count: padding_length)
            let prepared_data = zero_before + self.start_data[self.real_begin_time_step...]
            // let prepared_data = Array(self.start_data.suffix(98))
            self.filtered_data.append(contentsOf: delay_filters_package(prepared_data, use_more_data: true))
        }
    }
    
    public func obtain_filtered_data() -> [Double]{
        return self.filtered_data
    }
    
    public func calculate_zero_bias() -> Void{
        self.zero_begin_bias = Sigma.average(Array(self.start_data[self.real_begin_time_step..<self.real_begin_time_step + self.bias_estimation_window_size]))!
        // Then, make sure you apply the zero_begin_bias correctly
        for index in self.real_begin_time_step..<self.start_data.count{
            self.start_data[index] -= self.zero_begin_bias
        }
        print("zero begin bias calculated")
    }
    
    public func locate_real_start_point() -> Void{
        // As long as you are running this function, you haven't found the real start point
        if self.time_step > 500{
            print("Please move!!!")
        }
        
        let cur_stddev = Sigma.standardDeviationPopulation(Array(self.start_data[self.time_step..<self.time_step + 10]))
        if let stddev_num = cur_stddev{
            if stddev_num > 0.01{
                // At this point, we get the start point
                self.Found_real_start_point = true
                self.real_begin_time_step = self.time_step
                establish_start_data()
            }
        }
    }
    
    public func establish_start_data() -> Void{
        // After we find the start point, do something
        // Then we can calculate zero bias at here
        calculate_zero_bias()
        let prepared_data = Array(self.start_data[self.real_begin_time_step...])
        self.filtered_data.append(contentsOf: delay_filters_package(prepared_data, use_more_data: false))
    }
    
    public func delay_filters_package(_ prepared_data: [Double], use_more_data: Bool) -> [Double]{
        // Input data seq, return a filtered value
        // TODO: Notice this is a very slow version. Later I shall use some updated version to speed it up
        let first_level_averaged_data = apply_moving_agerage_three_times(prepared_data, use_more_data)
        let local_stddev = local_stddev_impl(first_level_averaged_data, use_more_data)
        let second_level_averaged_data = apply_moving_agerage_three_times(local_stddev, use_more_data)
        return second_level_averaged_data
    }
    
    public func apply_moving_agerage_three_times(_ input_data: [Double], _ use_more_data: Bool) -> [Double]{
        var result_data = [Double]()
        result_data = moving_average_impl(input_data, use_more_data)
        result_data = moving_average_impl(result_data, use_more_data)
        result_data = moving_average_impl(result_data, use_more_data)
        return result_data
    }
    
    public func moving_average_impl(_ input_data: [Double], _ use_more_data: Bool) -> [Double]{
        // Impl of the moving average on swift
        let half_window_size = self.mean_filter_window_size/2
        var average = [Double]()
        if use_more_data{
            // Also use previous available data when more previous data is available
            for index in half_window_size..<input_data.count - half_window_size{
                let current_data = input_data[index - half_window_size...index + half_window_size]
                average.append(Sigma.average(Array(current_data))!)
            }
        }
        else{
            for index in 0..<input_data.count - half_window_size{
                let current_data = input_data[max(index - half_window_size, 0)...index + half_window_size]
                average.append(Sigma.average(Array(current_data))!)
            }
        }
        return average
    }
    
    public func local_stddev_impl(_ input_data: [Double], _ use_more_data: Bool) -> [Double]{
        // Impl of the local standard deviation on swift
        let half_window_size = self.stddev_gen_window_size/2
        var stddev = [Double]()
        if use_more_data{
            // Also use previous available data when more previous data is available
            for index in half_window_size..<input_data.count - half_window_size{
                let current_data = input_data[index - half_window_size...index + half_window_size]
                stddev.append(Sigma.standardDeviationPopulation(Array(current_data))!)
            }
        }
        else{
            for index in 0..<input_data.count - half_window_size{
                let current_data = input_data[max(index - half_window_size, 0)...index + half_window_size]
                stddev.append(Sigma.standardDeviationPopulation(Array(current_data))!)
            }
        }
        return stddev
    }
}
