//
//  CSV_oper.swift
//  HMM_Test
//
//  Created by renpeng on 2/13/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation

class CSV_oper {
    init() { // Constructor
        
    }
    public func Read_in_CSV(file_path: String, row_num: Int) -> [String] {
        let stream = InputStream(fileAtPath: file_path)!
        let csv = try! CSVReader(stream: stream)
        var res_array:[String] = []
        while let row = csv.next() {
            res_array.append(row[row_num])
        }
        return res_array
    }
    public func Write_to_CSV(file_path: String, output_data: [Double]) -> Void{
        let stream = OutputStream(toFileAtPath: file_path, append: false)!
        let csv = try! CSVWriter(stream: stream)
        for data in output_data {
            try! csv.write(row: [String(data)])
        }
        csv.stream.close()
    }
}
