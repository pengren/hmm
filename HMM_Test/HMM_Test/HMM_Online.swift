//
//  HMMONLINE.swift
//  HMMONLINE
//
//  Created by ghflores on 4/3/17.
//  Copyright © 2017 UCSCCVLab. All rights reserved.
//

import Foundation

open class HMMONLINE: NSObject {
    
    //Private variables
    fileprivate var N:Int = 10//Number of states
    fileprivate var T:Int = 0//Number of observations
    fileprivate var A:[[Double]] = [[]]//Transition probabilities
    fileprivate var logA:[[Double]] = [[]]//Transition probabilities in log format
    fileprivate var statesSumNew:[Double] = []
    fileprivate var statesSumHistory:[Double] = []
    
    //Transition probabilities
//    fileprivate var P0:Double     = 0.998848
//    fileprivate var P45:Double    = 0.000150
//    fileprivate var P90:Double    = 0.000951
//    fileprivate var P180:Double   = 0.000050
//    fileprivate var Pd:Double     = 0.514765
//    fileprivate let delta:Double  = 0.00787383
//    fileprivate let sigma:Double  = 0.457895
    
    //As of 4/48
    fileprivate var P0:Double     = 0.9988839
    fileprivate var P45:Double    = 0
    fileprivate var P90:Double    = 0.000959 + 0.000151
    fileprivate var P180:Double   = 0.000050
    fileprivate var Pd:Double     = 0.490604
    fileprivate let delta:Double  = 0.00913624
    fileprivate let sigma:Double  = 0.352632
    
    //Transition probabilities (from set 1)
//    fileprivate var P0:Double     = 0.998853
//    fileprivate var P45:Double    = 0.0
//    fileprivate var P90:Double    = 0.001089
//    fileprivate var P180:Double   = 0.000057
//    fileprivate var Pd:Double     = 0.504027
//    fileprivate let delta:Double  = 0.0014953
//    fileprivate let sigma:Double  = 0.260357
    
    //HMM Viterbi variables
    fileprivate var States:[Double] = []
    fileprivate var O:[Double] = []
    fileprivate var time:[Double] = []
    fileprivate var statesPath:[Int] = []
    fileprivate var orientation:[Double] = []
    fileprivate var bestProb:Double = 0
    fileprivate var bias:Double = 0
    fileprivate var hmmReady:Bool = false
    
    //Standard initializer with no parameters. Must call setHMMObservations to set the azimuth and time data
    public override init() {
        super.init()
        
        N = 10//Number of states
        T = 0//Number of observations
        
        A = [[]]//Transition probabilities
        logA = [[]]//Transition probabilities in log format
        statesSumNew = []
        statesSumHistory = []
        
        States = []
        O = []
        time = []
        statesPath = []
        orientation = []
        //orientationWODelta = []
        bestProb = 0
        
        self.bias = 0
        self.hmmReady = false
    }
    
    //Initialize HMM model variables given a set of observations
    public init(_ observations:[Double], timeIntervals:[Double]) {
        super.init()
        
        if !observations.isEmpty {
            
            //Initialize this HMH model with the given observations
            self.initializeHMM(observations,time:timeIntervals)
            
            //Signal that the hmm is ready
            self.hmmReady = true
        }
    }
    
    //Set bias used in probOGivenS()
    public func setBias(_ bias:Double) -> Void {
        self.bias = bias
    }
    
    //Set observations and time. This initializes HMM model
    public func setHMMObservationsAndInitialize(_ observations:[Double], timeIntervals:[Double]) -> Void {
        
        if !observations.isEmpty {
            
            //Initialize this HMH model with the given observations
            self.initializeHMM(observations,time:timeIntervals)
            
            //Signal that the hmm is ready
            self.hmmReady = true
            
        } else {
            self.hmmReady = false
        }
    }
    
    //Set observations and time. This initializes HMM model
    public func setHMMObservations(_ observations:[Double], timeIntervals:[Double]) -> Void {
        
        if !observations.isEmpty {
            
            self.time = []
            self.O = []
            
            //Initialize this HMH model with the given observations
            self.time = timeIntervals
            self.O = HMMONLINE.unwrap(observations)//Unwrap the observations
            self.T = O.count
            self.statesPath = [Int](repeating:0, count: self.T)
            self.orientation = [Double](repeating:0, count: self.T)
            self.statesSumNew = [Double](repeating:0, count: N)
        }
    }
    
    //Initialize HMM probabilities, states given observations and time
    private func initializeHMM(_ observations:[Double], time:[Double]) -> Void {
        
        self.time = time
        self.O = HMMONLINE.unwrap(observations)//Unwrap the observations
        self.T = O.count
        self.statesPath = [Int](repeating:0, count: self.T)
        self.orientation = [Double](repeating:0, count: self.T)
        self.States = [0, Double.pi/4, Double.pi/2, (3/4)*Double.pi, Double.pi, -(3/4)*Double.pi, -Double.pi/2, -Double.pi/4, self.delta, -self.delta]
        self.statesSumNew = [Double](repeating:0, count: N)
        self.statesSumHistory = [Double](repeating:0, count: N)
        
        //Normalize probabilities
        let sumOfProbs:Double = self.P0+self.P45+self.P90+self.P45+self.P180+self.P45+self.P90+self.P45+self.Pd+self.Pd
        self.P0 = self.P0/sumOfProbs
        self.P45 = self.P45/sumOfProbs
        self.P90 = self.P90/sumOfProbs
        self.P180 = self.P180/sumOfProbs
        self.Pd = self.Pd/sumOfProbs
        
        //Normalized A
        self.A = [[self.P0,self.P45,self.P90,self.P45,self.P180,self.P45,self.P90,self.P45,self.Pd,self.Pd],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0],
                  [1,0,0,0,0,0,0,0,0,0]]
        
        //Build log of A
        self.logA = [[Double]](repeating:[Double](repeating:0,count:self.N), count: self.N)
        for row in 0...self.N-1 {
            for col in 0...self.N-1 {
                self.logA[row][col] = log(self.A[row][col])
            }
        }
    }
    
    //Call to run Viterbi algorithm
    public func runHHM() -> (statePath:[Int], orientationWODeltas:[Double]) {
        
        if self.hmmReady {
            //Run viterbi algorithm
            let results = viterbi()
            
            let statePath = results.finalStatesPath
            let orientation = results.finalOritentationWODeltas
            
            //Return results
            if !statePath.isEmpty {
                return (statesPath, orientation)
            } else {
                return ([],[])
            }
            
        } else {
            return ([],[])
        }
    }
    
    
    //Calculate emission probability. Using Gaussian distribution
    private func probOGivenS(x:Double, mu:Double, sigma:Double) -> Double {
        
        var gamma = x - mu
        
        if gamma < 0 {
            gamma = min(abs(x-mu), abs(x-mu+2*Double.pi))
        } else {
            gamma = min(abs(x-mu), abs(x-mu-2*Double.pi))
        }
        
        return log(exp(-((pow(gamma, 2))/(2*pow(sigma, 2)))) / (sqrt(2*Double.pi)*sigma))
    }
    
    //Viterbi algorithm
    private func viterbi() -> (finalStatesPath:[Int], finalOritentationWODeltas:[Double]) {
        
        var currentState = [Int](repeating:0, count: self.T)
        var pTR = [[Int]](repeating:[Int](repeating:0,count:self.T), count: self.N)//Store backpointers
        var statesSumPast = [[Double]](repeating:[Double](repeating:0,count:self.T), count: self.N)
        
        //Restore P(O|Sj,Sj-1,Sj-2...S0)
        for state in 0...self.N-1 {
            statesSumPast[state][0] = statesSumHistory[state]
        }
        
        //Model is in state 1 at step 0
        var v = [Double](repeating:-Double.infinity, count: self.N)
        v[0] = 0
        var vOld = v
        
        var bestVal:Double = 0
        var bestPTR:Int = 0
        var bestStateSum:Double = 0
        var inner:Int = 0
        var statesSum:Double = 0
        var B:Double = 0
        var val:Double = 0
        
        //Recursion step
        for t in 1...self.T-1 {
            for state in 0...self.N-1 {
                
                bestVal = -Double.infinity
                bestPTR = 0
                bestStateSum = 0
                
                //Only the first state 0 has all the transition probabilities. To avoid unnecessary calculations (multipliyng by 0 or -Inf), only the first column (state=0) in the transition probability matrix is iterated over all the states. For the rest of the columns, only the first one is used
                if state != 0 {
                    
                    inner = 0
                    
                    //P(O|Sj,Sj-1,Sj-2...S0)
                    statesSum = statesSumPast[inner][t-1] + self.States[state]
                    
                    //Calculate emission probability
                    B = self.probOGivenS(x: self.O[t]+self.bias, mu: statesSum, sigma: self.sigma)
                    
                    //Find the best values
                    val = vOld[inner] + self.logA[inner][state] + B
                    
                    if val > bestVal {
                        bestVal = val
                        bestPTR = inner
                        bestStateSum = statesSum
                    }
                    
                } else {
                    
                    for inner in 0...self.N-1 {
                        
                        //P(O|Sj,Sj-1,Sj-2...S0)
                        statesSum = statesSumPast[inner][t-1] + self.States[state]
                        
                        //Calculate emission probability
                        B = self.probOGivenS(x: self.O[t]+self.bias, mu: statesSum, sigma: self.sigma)
                        
                        //Find the best values
                        val = vOld[inner] + self.logA[inner][state] + B
                        
                        if val > bestVal {
                            bestVal = val
                            bestPTR = inner
                            bestStateSum = statesSum
                        }
                    }
                }
                
                //Save the best transition information for later backtracking
                pTR[state][t] = bestPTR
                v[state] = bestVal
                statesSumPast[state][t] = bestStateSum
            }
            
            vOld = v
        }
        
        //Save last //P(O|Sj,Sj-1,Sj-2...S0)
        for state in 0...self.N-1 {
            statesSumHistory[state] = statesSumPast[state][self.T-1]
        }
        
        //Decide which of the final states is post probable and save best probability
        let value = v.max()
        
        //If v is empty, value is nil
        if value != nil {
            bestProb = value!
            
            let finalState = v.index(where:{$0 == value})
            
            //Backtrace through the model
            currentState[self.T-1] = finalState!
            
            for t in (0...self.T-2).reversed() {
                currentState[t] = pTR[currentState[t+1]][t+1]
            }
            
            //Save the state path
            self.statesPath = currentState
            
            //Assuming orientation starts at 0, integrate over delta orientation to get the orientation
            for t in 1...self.T-1 {
                self.orientation[t] = self.orientation[t-1] + self.States[self.statesPath[t]]
            }
            
            var savedCurrentState = currentState
            
            //Remove the deltas
            for i in 0...savedCurrentState.count-1 {
                if ((savedCurrentState[i] == 8) || (savedCurrentState[i] == 9)) {
                    savedCurrentState[i] = 0
                }
            }
            
            var orientationWODelta = [Double](repeating:0, count: self.T)
            //Integrate without the deltas
            for t in 1...self.T-1 {
                orientationWODelta[t] = orientationWODelta[t-1] + self.States[savedCurrentState[t]]
            }
            
            //print(savedCurrentState)
            
            return (savedCurrentState,orientationWODelta)
            
            //If no optimal path, return empty array
        } else {
            
            return ([],[])
        }
    }
    
    //The following well tested C++ code for the MATLAB unwrap function was acquired from: https://www.medphysics.wisc.edu/~ethan/phaseunwrap/unwrap.c
    //Modified to have the size N rather than MAX_LENGTH=10000
    public class func unwrap(_ observations:[Double]) -> ([Double]) {
        
        var p = observations
        let N = observations.count
        
        var dp = [Double](repeating:0, count: N)
        var dps = [Double](repeating:0, count: N)
        var dp_corr = [Double](repeating:0, count: N)
        var cumsum = [Double](repeating:0, count: N)
        
        let cutoff = Double.pi
        
        //Incremental phase variation
        for j in 0...N-2 {
            dp[j] = p[j+1] - p[j]
        }
        
        //Phase variation in [-pi, pi]
        for j in 0...N-2 {
            dps[j] = (dp[j]+Double.pi) - floor( (dp[j]+Double.pi) / (2*Double.pi))*(2*Double.pi) - Double.pi
        }
        
        //Preserve variation sign for +pi vs. -pi
        for j in 0...N-2 {
            if ((dps[j] == -Double.pi) && (dp[j] > 0)) {
                dps[j] = Double.pi
            }
        }
        
        //Incremental phase correction
        //Phase variation in [-pi, pi]
        for j in 0...N-2 {
            dp_corr[j] = dps[j] - dp[j]
        }
        
        //Ignore correction when incremental variation is smaller than cutoff
        for j in 0...N-2 {
            if (fabs(dp[j]) < cutoff) {
                dp_corr[j] = 0
            }
        }
        
        //Find cumulative sum of deltas
        cumsum[0] = dp_corr[0]
        for j in 1...N-2 {
            cumsum[j] = cumsum[j-1] + dp_corr[j]
        }
        
        //Integrate corrections and add to P to produce smoothed phase values
        for j in 1...N-1 {
            p[j] = p[j] + cumsum[j-1]
        }
        
        return p
    }
    
}
