//
//  HMM_Online_test.swift
//  HMM_Test
//
//  Created by renpeng on 2/13/19.
//  Copyright © 2019 renpeng. All rights reserved.
//
// Test for HMM online

import Foundation
class HMM_Online_test {
    var file_path_prefix = ""
    var input_data:[Double]
    var input_time:[Double]
    var CSV_handler:CSV_oper
    var hmmOnlineModel:HMMONLINE = HMMONLINE()
    init(_ file_path: String) { // Constructor
        self.file_path_prefix = file_path
        self.input_data = []
        self.input_time = []
        self.CSV_handler = CSV_oper()
    }
    public func load_data() -> Void{
        let file_path = "/Users/renpeng/Documents/lstm_for_turn_detection/" + self.file_path_prefix
        // Load time
        self.input_time = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 0).map { Double($0)!}
        // Load data
        self.input_data = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 1).map { Double($0)!}
    }
    public func obtain_target_path() -> String{
        return "/Users/renpeng/Documents/HMM_res/" + self.file_path_prefix
    }
    public func run_HMM_Online() ->Void{
        var total_res_pred:[Int] = []
        var total_time_pred:[Int] = []
        var hmmFirstTime = true
        // Use following sentence to obtain "offline" test
        // let my_stride:Int = self.input_data.count - 1
        let my_stride:Int = 50
        // At each time, we obtain 50 length data & time
        for idx in stride(from: 0, to: self.input_data.count, by: my_stride) {
            if (idx+my_stride>=self.input_data.count){
                break
            }
            let cur_data = Array(self.input_data[idx..<idx+my_stride])
            let cur_time = Array(self.input_time[idx..<idx+my_stride])
            if (hmmFirstTime){
                // At first, obtain a global bias
                hmmFirstTime = false
                // Perform calculation
                //Find best alignment (e.g., w/ corridor)
                var currentIndex:Int = 0
                var bestBiases:[Int:Double] = [:]
                for bias in stride(from: -45, to: 50, by:5) {
                    currentIndex = currentIndex + 1
                    //Need independent HMM due to the threads
                    let tempHMM = HMM.init(cur_data, timeIntervals: cur_time)
                    tempHMM.setBias((Double.init(bias)*Double.pi/180.0))
                    
                    let hmmResults = tempHMM.runHHM()
                    let bestProb = hmmResults.bestProb
                    
                    bestBiases[bias] = bestProb
                }
                //Find best bias
                var minBias:Int = 0
                for (key,value) in bestBiases {
                    if value == bestBiases.values.max() {
                        minBias = key
                        break
                    }
                }
                //Initialize HMM and set HMM with best bias
                self.hmmOnlineModel.setHMMObservationsAndInitialize(cur_data, timeIntervals: cur_time)
                self.hmmOnlineModel.setBias((Double.init(minBias)*Double.pi/180.0))
            }
            else{
                self.hmmOnlineModel.setHMMObservations(cur_data, timeIntervals: cur_time)
            }
            // Run HMM
            let hmmResults = self.hmmOnlineModel.runHHM()
            // let cur_res = hmmResults.orientationWODeltas
            
            let processedTurns = processEstimatedOrientationOnline(orientation: hmmResults.orientationWODeltas, time_start_point: idx)
            
            let estimatedTurns:[Int] = processedTurns.estimatedTurns
            let estimatedTimes:[Int] = processedTurns.estimatedTimes
            
            total_res_pred.append(contentsOf: estimatedTurns)
            total_time_pred.append(contentsOf: estimatedTimes)
            // print("results: \(cur_res.count), total: \(total_res_pred.count)")
        }
        let real_res = return_offline_like_direction(orientation: total_res_pred, time: total_time_pred, total_length: self.input_data.count)
        // print(real_res)
        
        // print("count: \(self.input_data.count)")
        let output_file_path = obtain_target_path()
        self.CSV_handler.Write_to_CSV(file_path: output_file_path, output_data: real_res)
        print("Processed: \(output_file_path)")
    }
    //Process orientation to get turns taken and the time where these turns took place
    private func processEstimatedOrientationOnline(orientation:[Double], time_start_point: Int) -> (estimatedTurns:[Int], estimatedTimes:[Int]) {
        
        var turnsTaken:[Int] = []
        var turnsTimes:[Int] = []
        
        if !orientation.isEmpty {
            
            for i in 1...orientation.count-1 {
                let diff = orientation[i] - orientation[i-1]
                
                //Store only the turns and the times when they occurred
                if diff != 0 {
                    turnsTaken.append(Int(diff*180/Double.pi))
                    turnsTimes.append(i + time_start_point)
                }
            }
        }
        return (turnsTaken, turnsTimes)
    }
    private func return_offline_like_direction(orientation:[Int], time:[Int], total_length: Int) -> [Double]{
        var offline_res:[Double] = []
        var current_direction:Double = 0
        var index = 0
        var time_list = time
        time_list.append(total_length)
        
        if !orientation.isEmpty {
            
            for i in 0...total_length-1 {
                if (i<time_list[index]){
                    offline_res.append(current_direction)
                }
                else if (i == time_list[index]){
                    current_direction += Double(orientation[index])*Double.pi/180
                    index = index + 1
                    offline_res.append(current_direction)
                }
                else{
                    print("something wrong here")
                }
            }
        }
        // Update: This time we take the no turn situation into consideration
        else{
            for _ in 0...total_length-1 {
                offline_res.append(current_direction)
            }
        }
        return offline_res
    }
}
