//
//  MKF_util.swift
//  HMM_Test
//
//  Created by renpeng on 7/15/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
import simd

class MKF_util {
    init() {
        // Do nothing
    }
    public func obtain_real_angle_change(_ source: Double, _ target: Double) -> (Double){
        // Warning: this part only works for single input
        let angle_diff = target - source
        let true_angle_diff = (angle_diff + Double.pi).truncatingRemainder(dividingBy: (2*Double.pi)) - Double.pi
        return true_angle_diff
    }
    
    public func radius_to_string(_ radius: Double) -> (String){
        // turn radius to string so that we can get the corresponding probability
        var res = Int(4 * radius/Double.pi) * 45
        if(res == -180){
            res = 180
        }
        return String(res)
    }
    
    public func Choice_from_prob(_ candidate_list: [Double], _ prob_list: [Double], _ num_picks: Int) -> [Double]{
        // Draw choice from list with prob in prob_list, can be repeated
        var draw_res:[Double] = []
        for _ in 0..<num_picks {
            let prob_res_index = self.randomNumber(probabilities: prob_list)
            draw_res.append(candidate_list[prob_res_index])
        }
        return draw_res
    }
    
    public func randomNumber(probabilities: [Double]) -> Int {
        
        // Sum of all probabilities (so that we don't have to require that the sum is 1.0):
        let sum = probabilities.reduce(0, +)
        // Random number in the range 0.0 <= rnd < sum :
        let rnd = Double.random(in: 0.0 ..< sum)
        // Find the first interval of accumulated probabilities into which `rnd` falls:
        var accum = 0.0
        for (i, p) in probabilities.enumerated() {
            accum += p
            if rnd < accum {
                return i
            }
        }
        // This point might be reached due to floating point inaccuracies:
        return (probabilities.count - 1)
    }
    
    public func Normalize_dict(_ input_dict: [String: Double]) -> [String: Double]{
        // Normalize the dict, also known as, multiply each value by K_t
        let sum_value = input_dict.values.reduce(0, +)
        var output_dict: [String: Double] = [:]
        for (key, value) in input_dict {
            output_dict[key] = value/sum_value
        }
        return output_dict
    }
    
    public func calculate_COV(_ input_dict: [String: Double]) -> Double{
        // Given the array, calculate the coefficient_of_variation
        // get all the values
        let value_list = input_dict.values
        let mean = Sigma.average(Array(value_list))!
        let stddev = Sigma.standardDeviationPopulation(Array(value_list))!
        return stddev/mean
    }
    
    public func round_to_nearest_orientation(_ input_azimuth: Double) -> Double{
        // round the azimuth to the standard orientation
        return round(4 * input_azimuth/Double.pi) * Double.pi/4
    }
    
    public func wrap_azimuth_data(_ phases: Double) -> Double{
        // wrap the input azimuth data
        let res = ((-phases + Double.pi).truncatingRemainder(dividingBy: 2.0 * Double.pi) - Double.pi) * -1.0
        return res
    }
    
    public func obtain_wrapped_distance(Mean: Double, New_y: Double) -> Double{
        /*
         Given the mean, value, return the wrapped distance between Mean and New_y
         :param Mean: the proposed orientation with dirft
         :param New_y: y_value
         :return: wrapped diff between Mean and New_y
         */
        let wrapped_mean = self.wrap_azimuth_data(Mean)
        let real_distance = self.obtain_real_angle_change(wrapped_mean, New_y)
        return real_distance
    }
    
    public func drift_update_limiter(_ update_drift: [Double]) -> [Double]{
        // limit the dirft update size
        let sign_val = sign(update_drift[0])
        let limited_update_val = self.custom_sigmoid_func(abs(update_drift[0]))
        var result_drift = update_drift
        result_drift[0] = sign_val * limited_update_val
        return result_drift
    }
    
    public func custom_sigmoid_func(_ X: Double) -> Double{
        // smooth the limit curve
        let a = 1.67
        let b = -72.0
        let k = 0.001
        return k/(1 + exp(a + b*X))
    }
    
    public func normpdf(_ x: Double, _ mean: Double, _ sd: Double) -> Double{
        // Fast impl of pdf of gaussian...
        let variance = pow(sd, 2)
        let denom = pow((2.0 * Double.pi * variance), 0.5)
        let num = exp(-pow((x - mean), 2)/2 * variance)
        return num/denom
    }
}



