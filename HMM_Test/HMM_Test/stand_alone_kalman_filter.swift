//
//  stand_alone_kalman_filter.swift
//  HMM_Test
//
//  Created by renpeng on 7/16/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class stand_alone_kalman_filter{
    var transition_matrix:[String: Double] = [:]
    
    var orientation_candidate_list:[Double] = []
    
    var orientation_recorder:[Double] = [0.0]
    
    // Drift begins at zero
    var X: [[Double]] = []
    
    // the state-transition model
    var F: [[Double]] = []
    
    // observation model
    var H: [[Double]] = []
    
    // Error covariance
    var P: [[Double]] = []
    
    var Q: [[Double]] = []
    
    var R: Double = 0.0
    
    // KL info
    var KL_index = -1
    var KL_name = "Undefined KL"
    
    var mfk_util = MKF_util()
    
    init(_ KL_index: Int) {
        self.mkf_parameters_initialization()
        self.KL_index = KL_index
        self.KL_name = "KL_\(self.KL_index)"
        self.KL_parameters_initialization()
        
        
        
        
        
        
    }
    public func mkf_parameters_initialization(){
        // initialize parameters related to mkf
        self.transition_matrix = ["0": 0.98853,
                                "45": 0.005735,
                                "90": 0.0,
                                "135": 0.0,
                                "180": 0.0,
                                "-45": 0.005735,
                                "-90": 0.0,
                                "-135": 0.0
                                ]
        
        self.orientation_candidate_list = [0.0, Double.pi/4, -Double.pi/4, Double.pi/2, -Double.pi/2, 3*Double.pi/4, -3*Double.pi/4, Double.pi]
        
    }
    
    public func KL_parameters_initialization(){
        // initialize parameters related to the Kalman filter
        self.X = [[0.0], [1.0]]
        self.F = [[1.0, 0.0], [0.0, 1.0]]
        self.H = [[1.0, 0.0]]
        self.P = [[1.0, 0.0], [0.0, 0.0]]
        
        let sigma_w = 0.005
        let sigma_v = 0.1
        
        self.Q = [[sigma_w, 0.0], [0.0, 0.0]]
        self.R = pow(sigma_v, 2.0)
        
        var array3D: [[[Int]]] = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
