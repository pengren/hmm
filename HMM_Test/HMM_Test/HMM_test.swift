//
//  HMM_test.swift
//  HMM_Test
//
//  Created by renpeng on 2/13/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

// Test for HMM offline
import Foundation
class HMM_test {
    var file_path_prefix = ""
    var input_data:[Double]
    var input_time:[Double]
    var CSV_handler:CSV_oper
    init(_ file_path: String) { // Constructor
        self.file_path_prefix = file_path
        self.input_data = []
        self.input_time = []
        self.CSV_handler = CSV_oper()
    }
    public func load_data() -> Void{
        let file_path = "/Users/renpeng/Documents/lstm_for_turn_detection/" + self.file_path_prefix
        // Load time
        self.input_time = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 0).map { Double($0)!}
        // Load data
        self.input_data = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 1).map { Double($0)!}
    }
    public func obtain_target_path() -> String{
        return "/Users/renpeng/Documents/HMM_res/" + self.file_path_prefix
    }
    public func run_HMM() ->Void{
        //Find best alignment (e.g., w/ corridor)
        var currentIndex:Int = 0
        var bestBiases:[Int:Double] = [:]
        for bias in stride(from: -45, to: 50, by:5) {
                currentIndex = currentIndex + 1
                let tempHMM = HMM.init(self.input_data, timeIntervals: self.input_time)
                tempHMM.setBias((Double.init(bias)*Double.pi/180.0))
                
                let hmmResults = tempHMM.runHHM()
                let bestProb = hmmResults.bestProb
                bestBiases[bias] = bestProb
        }
        //Find best bias
        var minBias:Int = 0
        for (key,value) in bestBiases {
            
            if value == bestBiases.values.max() {
                minBias = key
                break
            }
        }
        //Run HMM-viterbi with best bias
        let hmmModel = HMM.init(self.input_data, timeIntervals: self.input_time)
        hmmModel.setBias((Double.init(minBias)*Double.pi/180.0))
        let hmmResults = hmmModel.runHHM()
        let res = hmmResults.orientationWODeltas
        let output_file_path = obtain_target_path()
        // print(res)
        self.CSV_handler.Write_to_CSV(file_path: output_file_path, output_data: res)
        
        print("Processed: \(output_file_path)")
    }
}
