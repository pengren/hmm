//
//  KL_Online_test.swift
//  HMM_Test
//
//  Created by renpeng on 7/2/19.
//  Copyright © 2019 renpeng. All rights reserved.
//

import Foundation
class KL_Online_test {
    var file_path_prefix = ""
    var input_data:[Double]
    var input_time:[Double]
    var CSV_handler:CSV_oper
    var start_data_length: Int // You first need this much data to 'start' the algorithm
    
    init(_ file_path: String) { // Constructor
        self.file_path_prefix = file_path
        self.input_data = []
        self.input_time = []
        self.CSV_handler = CSV_oper()
        self.start_data_length = 98
    }
    public func load_data() -> Void{
        let file_path = "/Users/renpeng/Documents/lstm_for_turn_detection/" + self.file_path_prefix
        // Load time
        self.input_time = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 0).map { Double($0)!}
        // Load data
        self.input_data = self.CSV_handler.Read_in_CSV(file_path: file_path, row_num: 1).map { Double($0)!}
        print("\(file_path) has been loaded")
    }
    public func obtain_target_path() -> String{
        return "/Users/renpeng/Documents/KL_res/" + self.file_path_prefix
    }
    public func preprocess_data() -> Void{
        // Make sure you preprocess the data!
        let dp = data_preprocessor(Array(self.input_data[0..<self.start_data_length]))
        for index in self.start_data_length..<self.input_data.count{
            dp.load_single_data(self.input_data[index])
            dp.process_input_data()
        }
        let filtered_data = dp.obtain_filtered_data()
        let turn_detecor = Turn_detector()
        let turn_label = turn_detecor.Intuitive_interval_detection(filtered_data)
        let output_file_path = obtain_target_path()
        self.CSV_handler.Write_to_CSV(file_path: output_file_path, output_data: turn_label)
        print("Processed: \(output_file_path)")
    }
}

